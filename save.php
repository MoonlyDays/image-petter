<?php
$hash = $_GET["url"];
$url = $_SERVER['DOCUMENT_ROOT']."/engine/cache/$hash.gif";

define("LIFETIME", 60 * 60 * 24);

if(file_exists($url) && filemtime($url) + LIFETIME > time())
{
    header("Content-type: ".mime_content_type($url));
    $fp = fopen($url, 'rb');
    fpassthru($fp);
}else {
    http_response_code(404);
    die("File not found or expired.");
}
echo file_get_contents($_SERVER["DOCUMENT_ROOT"]. "/templates/main.tpl");
?>
