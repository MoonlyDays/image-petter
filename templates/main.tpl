<head>

    <title>Pet your Image! by Moonly Days</title>

</head>
<body>

<h2>Pet your image. <img src="/assets/images/moonly.gif" height="30"></h2>
<p>Just upload the file and hit "Pet It!". It's that easy!<br/>
Note: Transparent images and images with full black (#000) may appear crappy.</p>
<form action="pet.php" method="post" enctype="multipart/form-data">
    <input type="file" accept="image/jpeg,image/x-png" name="image">
    <input type="submit" value="Pet It!">
</form>
<p>Or provide remote image url here:</p>
<form action="pet.php" method="get">
    <input type="url" placeholder="https://example.com/image.png" name="remote">
    <input type="submit" value="Pet It!">
</form>
<p>All generated media is kept online for 24 hours.<br/>By Moonly Days.</p>

</body>
