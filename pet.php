<?php

require_once $_SERVER['DOCUMENT_ROOT']."/engine/AnimGif.php";

function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
    // creating a cut resource
    $cut = imagecreatetruecolor($src_w, $src_h);

    // copying relevant section from background to the cut resource
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);

    // copying relevant section from watermark to the cut resource
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

    // insert cut resource to destination image
    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
}

function make_pet_part($src_hand, $src_face, $offset, $type)
{
    $sizes_hand = getimagesize($src_hand);  // Base hand image sizes

    $img_hand = imagecreatefrompng($src_hand);
    switch ($type) {
        case 'image/jpeg': $img_face = imagecreatefromjpeg($src_face); break;
        case 'image/png': $img_face = imagecreatefrompng($src_face); break;
        case 'image/webp': $img_face = imagecreatefromwebp($src_face); break;
        case 'text/plain': $img_face = imagecreatefromstring($src_face); break;
        default: return; break;
    }

    $sizes_face = [imagesx($img_face), imagesy($img_face)];         // Base face image sizes
    $sizes_face_dest = [$sizes_hand[0], $sizes_hand[1] - $offset];  // Desired face image sizes;

    // We create canvas with hand with dimensions.
    $canvas_hand = imagecreatetruecolor($sizes_hand[0], $sizes_hand[1]);
    imagealphablending($canvas_hand, false);
    imagesavealpha($canvas_hand, true);
    imagecopyresampled($canvas_hand, $img_hand, 0, 0, 0, 0, $sizes_hand[0], $sizes_hand[1], $sizes_hand[0], $sizes_hand[1]);

    // Create face canvas with proper dimensions -
    $canvas_face = imagecreatetruecolor($sizes_face_dest[0], $sizes_face_dest[1]);
    imagealphablending($canvas_face, false);
    imagesavealpha($canvas_face, true);
    imagecopyresampled($canvas_face, $img_face, 0, 0, 0, 0, $sizes_face_dest[0], $sizes_face_dest[1], $sizes_face[0], $sizes_face[1]);

    // Full canvas
    $canvas = imagecreatetruecolor($sizes_hand[0], $sizes_hand[1]);
    imagealphablending($canvas, false);
    imagesavealpha($canvas, true);

    $alpha = imagecolorallocate($canvas, 0, 0, 0);
    imagecolortransparent($canvas, $alpha);
    imagefill($canvas, 0, 0, $alpha);

    imagecopymerge_alpha($canvas, $canvas_face, 0, $offset, 0, 0, $sizes_hand[0], $sizes_hand[1], 100);
    imagecopymerge_alpha($canvas, $canvas_hand, 0, 0, 0, 0, $sizes_hand[0], $sizes_hand[1], 100);

    return $canvas;
}

$name = basename($_FILES["image"]["name"]);
$type = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

$src_base = $_FILES["image"]["tmp_name"];
$src_type = $_FILES["image"]["type"];
define("MAX_FILESIZE", 8388608);

$_REQ = $_GET ?? $_POST;

if(isset($_FILES["image"]))
{
    if($_SERVER["REQUEST_METHOD"] != "POST") {
        http_response_code(404);
        die();
    }

    if ($_FILES["image"]["size"] > MAX_FILESIZE) {
        http_response_code(400);
        die("File too large. Limit is 8MB.");
    }

    if(!in_array($_FILES["image"]["type"], ["image/png", "image/jpeg"])) {
        http_response_code(400);
        die("Invalid image type: only png, jpeg are allowed.");
    }
} else if(isset($_GET["remote"])) {

    if($_SERVER["REQUEST_METHOD"] != "GET") {
        http_response_code(404);
        die();
    }

    $url = $_GET["remote"];
    $url = explode("?", $_GET["remote"])[0];
    $ext = strtolower(pathinfo(basename($url), PATHINFO_EXTENSION));

    if(!in_array($ext, ["jpg", "png", "jpeg"]))
    {
        http_response_code(400);
        die("Invalid image type: only png, jpeg are allowed.");
    }
    $string = file_get_contents($_GET["remote"]);

    if(strlen($string) > MAX_FILESIZE) {
        http_response_code(400);
        die("File too large. Limit is 8MB.");
    }

    $src_base = $string;
    $src_type = "text/plain";
} else {
    http_response_code(400);
    die("No image provided.");
}

define("OFFSET_Y", [27, 33, 38, 41, 43, 43, 41, 38, 33, 27]);
define("FRAME_DURATION", 2);

$frames = [];
for ($i = 1; $i <= 10; $i++) {
    $src_hand = $_SERVER['DOCUMENT_ROOT']."/assets/images/pet/hand_$i.png";

    $frame = make_pet_part($src_hand, $src_base, OFFSET_Y[$i-1], $src_type);
    array_push($frames, $frame);
}

$durations = array_fill(0, 10, FRAME_DURATION);

$anim_gif = new GifCreator\AnimGif();
$gif_object = $anim_gif->create($frames, $durations, 0);
$gif_binary = $gif_object->get();

$hash = [];
for($i = 0; $i < 4; $i++)
{
    array_push($hash, substr(sha1($src_base.$i), 0, 4));
}
$hash = join($hash, "_");
$name = $_SERVER['DOCUMENT_ROOT']."/engine/cache/$hash.gif";

$file = fopen($name, "w");
fwrite($file, $gif_binary);
fclose($file);

if($_REQ["api"] == "true") {
    die("https://".$_SERVER['SERVER_NAME']."/pet/$hash.gif");
} else if($_REQ["response"] == "true") {
    header("Content-type: image/gif");
    echo $gif_binary;
} else {
    header("Location: /pet/$hash.gif");
}
?>
